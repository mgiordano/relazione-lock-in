CARTELLA	= 3-lock-in
MAIN		= relazione3
MAIN_PDF	= $(MAIN).pdf
MAIN_TEX	= $(MAIN).tex
CLEAN_FILE	= *.aux *.fdb_latexmk *.log *~ .*~
DISTCLEAN_FILE	= $(MAIN_PDF)

.PHONY: pdf clean distclean dist

pdf: $(MAIN_PDF)

$(MAIN_PDF): $(MAIN_TEX)
	latexmk -pdf $(MAIN)

clean:
	rm -f $(CLEAN_FILE)

distclean: clean
	rm -f $(DISTCLEAN_FILE)

dist: distclean
	git gc # comprimo il repository di git per ridurre al minimo la tarball
	cd .. && tar -cJvpsf $(CARTELLA).tar.xz --exclude=$(CARTELLA)/auto $(CARTELLA)/
