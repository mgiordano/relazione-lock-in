\documentclass[a4paper,fleqn,twoside]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}

\usepackage[light]{kpfonts}
\usepackage{booktabs,layaureo,siunitx}
\usepackage[siunitx]{circuitikz}
\usepackage[font=small,format=hang,labelfont=bf]{caption}

\ctikzset{bipoles/length=.8cm}
\usetikzlibrary{decorations.pathreplacing}

\sisetup{per-mode=symbol,
  inter-unit-separator={}\cdot{},
  exponent-product=\cdot,
  output-product=\cdot,
  separate-uncertainty=true
}

% Costante di Eulero
\DeclareMathOperator{\e}{\mathrm{e}}

\author{Marta \\ Dell'Atti \and{} Mosè \\ Giordano \and{} Matteo \\ Leo \and{}
  Martina \\ Montinaro}
\title{Relazione sul circuito di lock-in \\[0.5ex]
  \large{Corso di Laurea Magistrale in Fisica}}
\date{a. a. 2011-2012}

\begin{document}
\maketitle{}

\section{Primo blocco: adattatore e sfasatore di $\pi/2$}
\label{sec:primo-blocco}

\begin{figure}
  \centering
  \begin{circuitikz}[scale=0.7,font=\footnotesize]
    \draw (0,0) node[op amp] (opamp1) {}; % primo Op Amp
    % resistenza R_6 e generatore di funzioni
    \draw (opamp1.-) to[R, l_=$R_{6}$] ++(-2,0) to[sinusoidal current source]
          ++(-2,0) node[ground] {};
    % massa del primo Op Amp
    \draw (opamp1.+) node[above] {$V_{+1}$} to ++(0,-0.5) node[ground] {};
    % resistenza di reazione R_7
    \draw (opamp1.-) node[below] {$V_{-1}$} -- ++(0,1) to[R, l=$R_{7}$] ++(3,0)
          -| (opamp1.out);
    \draw (opamp1.out) |- ++(1,-1) node[right] {$S_{2}$}; % segnale S_2
    \draw (opamp1.out) |- ++(1,1.7) node[shape=coordinate] (A) {};
    % posiziono a occhio il secondo Op Amp
    \draw ($(opamp1.out) + (5,1.7)$) node[op amp] (opamp2) {};
    % resistenza R_9
    \draw (opamp2.-) node[below] {$V_{-2}$} to[R, l_=$R_{9}$] ++(-2,0) -| (A);
    % condensatore C_3
    \draw (opamp2.+) node[above] {$V_{+2}$}  to[C, l=$C_{3}$] ++(-2,0) -| (A);
    % resistenze R_8 e R_trim
    \draw (opamp2.+) to[R, l=${R_{8}}$, i>_=$I_{2}$] ++(0,-2)
          to[vR, l_=$R_{\textup{trim}}$] ++(2,0) node[ground] {};
    % resistenza di reazione R_10
    \draw (opamp2.-) -- ++(0,1) to[R, l=$R_{10}$, i>^=$I_{1}$] ++(3,0) -|
          (opamp2.out);
    \draw (opamp2.out) -- ++(1,0) node[right] {$S_{1}$}; % segnale S_1

    % indico i vari stadi con delle parentesi graffe
    \draw[decorate,decoration={brace,mirror}] ($(opamp1.-)-(2,3)$) --
         node[below] {adattatore} ($(opamp1.out)-(0,2.3)$);
    \draw[decorate,decoration={brace,mirror}] ($(A)-(0,4)$) --
         node[below] {sfasatore di $\pi/2$} ($(opamp2.out)-(0,4)$);
  \end{circuitikz}
  \caption{Schema dell'adattore e dello sfasatore di $\pi/2$.}
  \label{fig:blocco1}
\end{figure}
Per montare l'adattatore e lo sfasatore (figura~\ref{fig:blocco1}) abbiamo
utilizzato componenti con le seguenti grandezze misurate con il multimetro a
nostra disposizione:
\begin{gather*}
  R_{6} = \SI{68.0}{\kilo\ohm}, \\
  R_{7} = \SI{47.1}{\kilo\ohm}, \\
  R_{8} = \SI{9.99}{\kilo\ohm}, \\
  R_{9} = R_{10} = \SI{21.9}{\kilo\ohm}.
\end{gather*}
Inoltre abbiamo usato un condensatore con capacità nominale
$C_{3} = \SI{10}{\nano\farad}$.

Abbiamo impostato sul generatore di funzioni una frequenza $f$ per il segnale di
ingresso, misurata mediante l'oscilloscopio, di $f = \SI{1.028}{\kilo\hertz}$ e,
sempre grazie all'oscilloscopio, abbiamo trovato
\begin{gather*}
  \text{segnale $S_{2}$: forma sinusoidale, ampiezza picco-picco: }
  \SI{10.2}{\volt}, \\
  \text{segnale $S_{1}$: forma sinusoidale, ampiezza picco-picco: }
  \SI{10.3}{\volt}.
\end{gather*}
Abbiamo variato la resistenza di trimmer $R_{\textup{trim}}$ con un cacciavite
fino a raggiungere lo sfasamento di $\pi/2$ fra i segnali $S_{1}$ e $S_{2}$.

Rilevando con le sonde dell'oscilloscopio i segnali $V_{-2}$ e $V_{+2}$ abbiamo
verificato che questi non fossero sfasati, come ci aspettavamo dal principio di
massa virtuale.

\section{Secondo blocco: sfasatore variabile e squadratore}
\label{sec:secondo-blocco}

\begin{figure}
  \centering
  \begin{circuitikz}[scale=0.7,font=\footnotesize]
    \draw (-0.8,0) node[spdt,rotate=180] {};
    \draw (-1.6,0.5) node[left] {$S_{1}$}
          (-1.6,-0.5) node[left] {$S_{2}$};
    \draw (0,0) -- ++(0.5,0) node[shape=coordinate] (A) {};
    % posiziono a occhio l'Op Amp
    \draw (5,0) node[op amp] (opamp) {};
    % resistenza R_11
    \draw (opamp.-) node[below] {$V_{-}$} to[R, l_=$R_{11}$] ++(-2,0) -| (A);
    % condensatore C_4
    \draw (opamp.+) node[above] {$V_{+}$}  to[C, l=$C_{4}$] ++(-2,0) -| (A);
    % resistenze R_13 e R_pot
    \draw (opamp.+) to[R, l=${R_{13}}$] ++(0,-2.5)
          to[vR, l=$R_{\textup{pot}}$] ++(0,-1.5) node[ground] {};
    % resistenza di reazione R_12 e condensatore C_5
    \draw (opamp.-) -- ++(0,1) to[R, l=$R_{12}$] ++(3,0) -| (opamp.out)
                    node[below] {$V_{3}$} to[C=$C_{5}$] ++(2,0)
                    node[shape=coordinate] (P) {};
    \node[pnp] at ($(P) + (1.5,0)$) (pnp) {}; % posiziono a occhio il transistor
    \draw (P) node[below left] {$V_{5}$} -- (pnp.B) node[above right] {B};
    % resistenza R_14
    \draw (P) to[R, l_=$R_{14}$] ++(0,2.5) node[shape=coordinate](N) {} -|
          (pnp.E) node[below right] {E};
    % resistenza R_15
    \draw (P) to[R, l_=$R_{15}$] ++(0,-3) node[shape=coordinate] (vcc) {};
    % resistenza R_16 e generatore di -V_cc = -15 V
    \draw (pnp.C) node[above left] {C} to[R, l=$R_{16}$, i>_=$I^{*}$] ++(0,-2)
          -| (vcc) to[battery=-15<\volt>] ++(-2,0) node[ground] {};
    \draw (pnp.C) to[R=$R_{17}$] ++(3,0) node[shape=coordinate] (Q) {} --
          ++(1,0) node[right] {$S_{3}$};
    \draw (Q) to[R=$R_{18}$] ++(0,3.5) |- (N); % resistenza R_18
    \node[ground] at ($(N) + (3,0)$) {}; % massa dell'emettitore del transistor

    % indico i vari stadi con delle parentesi graffe
    \draw[decorate,decoration={brace,mirror}] ($(A)-(0,6)$) --
         node[below] {sfasatore variabile} ($(opamp.out)-(0,6)$);
    \draw[decorate,decoration={brace,mirror}] ($(P)-(0,6)$) --
         node[below] {squadratore} ++(2,0);
    \draw[decorate,decoration={brace,mirror}] ($(P)+(2,-6)$) --
         node[below,align=center,text width=2cm]  {partitore di tensione}
         ++(3.5,0);
\end{circuitikz}
  \caption{Schema dello sfasatore variabile e dello squadratore.}
  \label{fig:blocco2}
\end{figure}
Per montare lo sfasatore variabile e lo squadratore (figura~\ref{fig:blocco2})
abbiamo utilizzato componenti con le seguenti grandezze misurate con il
multimetro a nostra disposizione:
\begin{gather*}
  R_{11} = R_{12} = \SI{26.9}{\kilo\ohm}, \\
  R_{13} = \SI{0.996}{\kilo\ohm}, \\
  R_{14} = \SI{15.02}{\kilo\ohm}, \\
  R_{15} = \SI{9.90}{\kilo\ohm}, \\
  R_{16} = \SI{33.0}{\kilo\ohm}, \\
  R_{17} = \SI{32.9}{\kilo\ohm}, \\
  R_{18} = \SI{117.6}{\kilo\ohm}.
\end{gather*}
Abbiamo usato condensatori con capacità nominali di
$C_{4} = \SI{10}{\nano\farad}$ e $C_{5} = \SI{0.1}{\micro\farad}$.

Il condensatore di capacità $C_{5}$ serve per disaccoppiare i due stadi e funge
inoltre da filtro passa-alto, in modo da eliminare eventuali componenti costanti
nel segnale di ingresso.

Rilevando con l'oscilloscopio i segnali $S_{2}$ e $V_{3}$ abbiamo verificato che
l'ampiezza di $V_{3}$ non varia modificando il valore della resistenza
$R_{\textup{pot}}$ ma varia soltanto la fase relativa al segnale di riferimento
$S_{2}$, come ci aspettavamo.  L'ampiezza di $V_{3}$ è uguale a quella di
$S_{1}$ e $S_{2}$ poiché lo stadio di sfasatore variabile sfasa il segnale di
ingresso senza amplificarlo.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{V5}
  \caption{Andamento temporale del segnale $V_{5}$ rilevato con l'oscilloscopio.}
  \label{fig:v5}
\end{figure}
Abbiamo rilevato con l'oscilloscopio il segnale $V_{5}$ e abbiamo trovato
l'andamento temporale visibile nella figura~\ref{fig:v5}.  La tensione massima
negativa è \SI{-0.80}{\volt}, quella positiva è \SI{+2.60}{\volt}.  Variando il
valore della resistenza $R_{\textup{pot}}$ varia solo la fase e non l'ampiezza
del segnale $V_{5}$.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{squadratore}
  \caption{Andamento temporale del segnale $S_{3}$ di uscita dello squadratore
    confrontato con quello del segnale $V_{5}$ di ingresso.}
  \label{fig:squadratore}
\end{figure}
Nella figura~\ref{fig:squadratore} sono mostrati gli andamenti temporali dei
segnali di ingresso $V_{5}$ (forma sinusoidale) e di uscita $S_{3}$ dello
squadratore, rilevati con l'oscilloscopio.  Il \emph{duty-cicle} $d$ dell'onda
quadra è il rapporto fra la durata $a$ del livello alto dell'onda e il periodo
$b$ totale.  Abbiamo misurato queste grandezze sull'oscilloscopio e abbiamo
trovato $a \approx \SI{500}{\micro\second}$ e $b \approx \SI{1}{\milli\second}$,
quindi $d = a/b \approx 0.5 = 50\%$ che è il valore ottimale per il duty-cicle
affinché l'intero circuito lavori correttamente.  Il valore massimo del segnale
$S_{3}$ è \SI{0}{\volt}, quello minimo \SI{-10}{\volt}, dunque l'ampiezza
picco-picco è \SI{10}{\volt}.  Il segnale $S_{3}$ andrà in ingresso al gate del
J-FET del rivelatore di fase (figura~\ref{fig:blocco4}), il quale può sopportare
una tensione massima di modulo \SI{12}{\volt}, quindi siamo in condizioni di
lavoro ottimali dato che il valore massimo della tensione di $S_{3}$ è in modulo
\SI{10}{\volt}.
% Nota: S_3 è preso all'uscita del partitore con le resistenze R_17 e R_18
% proprio per realizzare questa condizione.

\section{Terzo blocco: amplificatore di potenza e preamplificatore}
\label{sec:terzo-blocco}

\begin{figure}
  \centering
  \begin{circuitikz}[scale=0.7,font=\footnotesize]
    \draw (0,0) node[shape=coordinate] (A) {} to[R, l=$R_{19}$] ++(0,2)
          node[above] {$S_{2}$}; % resistenza R_19
    % resistenza R_20
    \draw (A) node[left] {A} to[R, l_=$R_{20}$] ++(0,-2) node[ground] {};
    \node[npn] at ($(A) + (3,0)$) (npn) {}; % posiziono a occhio il transistor
    % condensatore C_6 e resistenza R_21
    \draw (A) to[C, l=$C_{6}$] (npn.B) node[above right] {B} to[R, l_=$R_{21}$]
          ++(0,3) node[shape=coordinate] (Z) {};
    % led, resistenza R_22 e massa
    \draw (npn.E) node[above right] {E} to[empty led] ++(0,-1.5) node[left] {D}
          to[R, l_=$R_{22}$] ++(0,-1.5) node[ground] {};
    \draw (npn.C) node[below right] {C} |- (Z) -- ++(3,0)
          node[shape=coordinate] (Y) {};
    \node[npn] at ($(Y) - (0,5)$) (phtr) {}; % posiziono a occhio fototransistor
    % in mancanza del simbolo corretto per il fototransistor disegno manualmente
    % una circonferenza intorno a un npn.  In realtà la circonferenza non è
    % molto importante, dovrei proprio far fuori la base.
    \draw ($(Y)-(0.7,5)$) circle (0.5);
    \draw ($(Y)+(2,0)$) node[ground] {} to[battery=+15<\volt>] (Y) -- (phtr.C);
    \draw (phtr.E) to[R,l_=$R_{23}$] ++(0,-1) node[ground] {}; % resistenza R_23
    \draw (phtr.E) node[above right] {F} to[C,l=$C_{7}$] ++(2,0)
          node[shape=coordinate] (H) {};
    \draw (H) node[above left] {H} to[R, l=$R_{24}$] ++(0,-1) node[ground] {};
    \node[op amp] at ($(H)+(3,3)$) (opamp) {}; % posiziono a occhio l'Op Amp
    \draw (H) |- (opamp.+);
    % resistenza R_25
    \draw (opamp.-) node[below] {G} to[R, l=$R_{25}$] ++(-2,0) node[ground] {};
    \draw (opamp.-) -- ++(0,2) to[R, l=$R_{26}$] ++(3,0) -| (opamp.out) --
          ++(0.5,0) node[right] {$S_{4}$}; % resistenza di rezione R_26
    \draw (opamp.up) -- ++(0,0.5) node[above] {\SI{+15}{\volt}}; % +V_cc
    \draw (opamp.down) -- ++(0,-0.5) node[below] {\SI{-15}{\volt}}; % -V_cc

    % indico i vari stadi con delle parentesi graffe
    \draw[decorate,decoration={brace,mirror}] ($(A)-(0,5)$) --
         node[below,align=center,text width=2cm] {amplificatore di potenza}
         ++(3,0);
    \draw[decorate,decoration={brace,mirror}] ($(opamp.-)-(1,5.7)$) -- node[below]
    {preamplificatore} ($(opamp.out)-(0,5)$);
\end{circuitikz}
  \caption{Schema dell'amplificatore di potenza e del preamplificatore.}
  \label{fig:blocco3}
\end{figure}
Per montare l'amplificatore di potenza e il preamplificatore
(figura~\ref{fig:blocco3}) abbiamo utilizzato componenti con le seguenti
grandezze misurate con il multimetro a nostra disposizione:
\begin{gather*}
  R_{19} = \SI{14.9}{\kilo\ohm}, \\
  R_{20} = \SI{1.49}{\kilo\ohm}, \\
  R_{21} = \SI{265}{\kilo\ohm}, \\
  R_{22} = \SI{55.3}{\ohm}, \\
  R_{23} = \SI{14.9}{\kilo\ohm}, \\
  R_{24} = \SI{1.193}{\mega\ohm}, \\
  R_{25} = \SI{9.99}{\kilo\ohm}, \\
  R_{26} = \SI{567}{\kilo\ohm}.
\end{gather*}
Abbiamo usato anche due condensatori con capacità nominali di
$C_{6} = C_{7} = \SI{0.1}{\micro\farad}$.

Con l'oscilloscopio abbiamo rilevato la tensione in alcuni dei punti mostrati
nella figura~\ref{fig:blocco3} usando il segnale $S_{2}$ (che ha un'ampiezza
picco-picco di \SI{10.2}{\volt}) come riferimento.  Abbiamo trovato
\begin{itemize}
\item A: forma sinusoidale, in fase con $S_{2}$, ampiezza picco-picco =
  \SI{860}{\milli\volt}.
\item B: forma sinusoidale, in fase con $S_{2}$, ampiezza picco-picco =
  \SI{848}{\milli\volt};
\item E: forma sinusoidale, in fase con $S_{2}$, ampiezza picco-picco =
  \SI{840}{\milli\volt};
\item D: forma sinusoidale, in fase con $S_{2}$, ampiezza picco-picco =
  \SI{800}{\milli\volt};
\item F: forma sinusoidale, segnale molto rumoroso perché è l'emettitore del
  fototransistor che sta assorbendo anche la luce ambientale oltre a quella del
  LED.  Il segnale è leggermente sfasato rispetto a $S_{2}$, ampiezza
  picco-picco = \SI{1.30}{\volt};
\item G: forma sinusoidale, segnale molto rumoroso, in fase con $S_{2}$,
  ampiezza picco-picco = \SI{1.35}{\volt};
\item H: forma sinusoidale, segnale molto rumoroso, in fase con $S_{2}$,
  ampiezza picco-picco = \SI{1.35}{\volt}.  Ci aspettavamo che questo punto
  presentasse le stesse caratteristiche del punto G per il principio della massa
  virtuale dell'amplificatore operazionale;
\item $S_{4}$: forma di sinusoide tagliata, segnale molto rumoroso.  Il segnale
  di ingresso $V_{\textup{H}}$ dell'amplificatore non invertente ha un'ampiezza
  di \SI{1.35}{\volt} e l'amplificazione di questo stadio è
  $1+R_{26}/R_{25} \approx 58$, quindi l'amplificatore operazionale è in
  saturazione (l'ampiezza massima che l'operazionale può produrre in uscita è
  circa \SI{30}{\volt}) e il segnale di uscita $S_{4}$ risulta essere tagliato.
  Per poter verificare che la forma d'onda di uscita del preamplificatore fosse
  effettivamente una sinusoide abbiamo sostituito al resistore $R_{26}$ un altro
  resistore con resistenza, misurata con il multimetro, di
  $R'_{26} = \SI{99.9}{\kilo\ohm}$ in modo che l'amplificazione diventi
  $1+R'_{26}/R_{25} = 11$ e rimanere nella regione di funzionamento lineare
  dell'operazionale.  Dopo aver effettuato la sostituzione abbiamo
  effettivamente trovato che $S_{4}$ ha la forma di una sinusoide.
\end{itemize}

La corrente $I_{\textup{B}}$ che entra nella base del transistor
dell'amplificatore è data da
$I_{\textup{B}} = (\SI{15}{\volt}-V_{\textup{B}})/R_{21}$.  La tensione
$V_{\textup{B}}$ nel punto B è variabile, è un seno, ed è data dalla somma della
corrente alternata che proviene da $S_{2}$ modulata dal condensatore $C_{6}$ e
della corrente continua che proviene dal ramo del resistore $R_{21}$.  Abbiamo
deciso di utilizzare come stima di $V_{\textup{B}}$ la sua media.  Per fare
questo abbiamo impostato sull'oscilloscopio l'accoppiamento DC (in questo modo
vengono visualizzati direttamente i valori di tensione letti dall’oscilloscopio)
invece di AC (in questo modo vengono visualizzati i valori di tensione letti
dall'oscilloscopio a cui viene sottratto il valore medio del segnale, dunque la
media è sempre nulla) e abbiamo trovato che la media di $V_{\textup{B}}$ vale
\SI{2.90}{\volt}, quindi
$I_{\textup{B}} = (15-2.90)\si{\volt}/\SI{265}{\kilo\ohm} =
\SI{0.046}{\milli\ampere}$.
Il massimo valore di corrente di base sopportabile dal transistor è
\SI{7.0}{\milli\ampere} quindi siamo in condizioni di lavoro ottimali.

Per valutare lo sfasamento $\Delta$ fra i segnali $S_{4}$ e $V_{\textup{F}}$ li
abbiamo rilevati con l'oscilloscopio e misurato la distanza temporale $\Delta t$
fra due massimi adiacenti dei due segnali.  Poiché entrambi questi segnali sono
molto rumorosi, per poter effettuare una misura più attendibile abbiamo coperto
la basetta in modo da ridurre la luce ambientale che entra nel fototransistor.
In questo modo abbiamo trovato $\Delta t \approx \SI{0.5}{\micro\second}$, quindi
$\Delta = \Delta t \cdot f \cdot 2\pi = \SI{0.5}{\micro\second} \cdot
\SI{1.028}{\kilo\hertz} \cdot 2\pi \approx \SI{0.03}{rad}$.

\section{Quarto blocco: rivelatore di fase e integratore}
\label{sec:quarto-blocco}

\begin{figure}
  \centering
  \begin{circuitikz}[scale=0.7,font=\footnotesize]
    \draw (0,0) node[shape=coordinate] (A) {} -- ++(-0.5,0) node[left]{$S_{4}$};
    \node[op amp] at ($(A)+(6,0)$) (opamp1) {}; % posiziono a occhio l'Op Amp 1
    % resistenza R_27
    \draw (opamp1.-) node[below] {$V_{1-}$} to[R, l_=$R_{27}$] ++(-4,0) -| (A)
          node[right] {A};
    % resistenze R_28 e R_29
    \draw (opamp1.+) node[above] {$V_{1+}$} to[R, l=$R_{29}$] ++(-2,0)
          node[shape=coordinate] (B) {} to[R, l=$R_{28}$] ++(-2,0) -| (A);
    \node[njfet] at ($(B)+(0,-2)$) (njfet) {}; % posiziono a occhio il J-FET n
    \draw (B) node[above] {B} -- (njfet.D) node[below right] {D};
    % ingresso del J-FET
    \draw (njfet.G) node[above] {G} -- ++(-1,0) node[left] {$S_{3}$};
    % massa del J-FET
    \draw (njfet.S) node[above right] {S} node[ground] {};
    % resistenza di reazione R_30
    \draw (opamp1.-) -- ++(0,1) to[R, l=$R_{30}$] ++(3,0) -| (opamp1.out)
          node[below] {C};
    % posiziono a occhio l'Op Amp 2
    \node[op amp] at ($(opamp1.out)+(6,-0.7)$) (opamp2) {};
    \draw (opamp1.out) to[R, l=$R_{31}$] ++(2,0) node[shape=coordinate] (E) {}
          to[R, l=$R_{32}$] (opamp2.-) node[below] {$V_{2-}$};
    \draw (E) node[above] {E} to[C, l=$C_{8}$] ++(0,-2) node[ground] {};
    \draw (opamp2.+) node[above] {$V_{2+}$} to[R, l_=$R_{33}$] ++(0,-2)
          node[ground] {};
    % resistenza R_34
    \draw (opamp2.-) -- ++(0,2) to[R, l_=$R_{34}$] ++(3,0) -| (opamp2.out);
    % condensatore C_9
    \draw (opamp2.-) -- ++(0,3) to[C, l=$C_{9}$] ++(3,0) -| (opamp2.out);
    % segnale di uscita
    \draw (opamp2.out) -- ++(0.5,0) node[right] {$V_{\textup{out}}$};
    \draw (opamp2.up) -- ++(0,0.5) node[above] {\SI{+15}{\volt}}; % +V_cc
    \draw (opamp2.down) -- ++(0,-0.5) node[below] {\SI{-15}{\volt}}; % -V_cc

    % indico i vari stadi con delle parentesi graffe
    \draw[decorate,decoration={brace,mirror}] (0,-5) -- node[below] {rivelatore
      di fase} ($(opamp1.out)-(0,5)$);
    \draw[decorate,decoration={brace,mirror}] ($(opamp1.out)-(0,5)$) --
         node[below,align=center,text width=2cm] {filtro passa-basso}
         ($(E)-(0,5)$);
    \draw[decorate,decoration={brace,mirror}] ($(E)-(0,5)$) -- node[below]
         {integratore} ($(opamp2.out)-(0,4.3)$);
  \end{circuitikz}
  \caption{Schema del rivelatore di fase e dell'integratore.}
  \label{fig:blocco4}
\end{figure}
Per montare il rivelatore di fase e l'integratore (figura~\ref{fig:blocco4})
abbiamo utilizzato componenti con le seguenti grandezze misurate con il
multimetro a nostra disposizione:
\begin{gather*}
  R_{27} = \SI{68.0}{\kilo\ohm}, \\
  R_{28} = \SI{21.9}{\kilo\ohm}, \\
  R_{29} = \SI{22.0}{\kilo\ohm}, \\
  R_{30} = \SI{68.1}{\kilo\ohm}, \\
  R_{31} = \SI{9.9}{\kilo\ohm}, \\
  R_{32} = \SI{1.54}{\mega\ohm}, \\
  R_{33} = \SI{1.18}{\mega\ohm}, \\
  R_{34} = \SI{3.30}{\mega\ohm}.
\end{gather*}
Abbiamo usato anche due condensatori con capacità nominali di
$C_{8} = \SI{1.5}{\nano\farad}$ e $C_{9} = \SI{0.22}{\micro\farad}$.

\begin{table}
  \centering
  \caption{Dati dell'attenuazione ottica.  $n$ è il numero di fogli di carta
    inseriti fra il LED e il fototransistor.}
  \label{tab:dati}
  \begin{tabular}{SSSS}
    \toprule{}
    {$n$} & {$V_{\textup{out}}$ (\si{\volt})} & {scala (\si{\volt})} &
    {incertezza (\si{\volt})} \\
    \midrule{}
    0  & 10.5 & 5   & 0.5  \\
    1  & 5.6  & 2   & 0.2  \\
    2  & 3.2  & 1   & 0.1  \\
    3  & 2.2  & 1   & 0.1  \\
    4  & 1.80 & 0.5 & 0.05 \\
    5  & 1.40 & 0.5 & 0.05 \\
    6  & 1.20 & 0.5 & 0.05 \\
    7  & 1.00 & 0.5 & 0.05 \\
    8  & 0.90 & 0.5 & 0.05 \\
    9  & 0.64 & 0.5 & 0.05 \\
    10 & 0.52 & 0.5 & 0.05 \\
    11 & 0.40 & 0.2 & 0.02 \\
    12 & 0.32 & 0.2 & 0.02 \\
    13 & 0.28 & 0.2 & 0.02 \\
    \bottomrule{}
  \end{tabular}
\end{table}
Dopo aver completato la realizzazione del circuito abbiamo effettuato le misure
di attenuazione ottica: abbiamo posto dei fogli di carta fra il LED e il
fototransistor e misurato con l'oscilloscopio l'andamento della tensione di
uscita $V_{\textup{out}}$ dell'integratore in funzione del numero di fogli
inseriti.  Poiché l'inserimento di un foglio comporta lo sfasamento del segnale
che giunge nel fototransistor, prima di effettuare ciascuna misura abbiamo
commutato l'interruttore prima dello sfasatore variabile su $S_{1}$, variato
$R_{\textup{pot}}$ in modo da ottenere una tensione di uscita $V_{\textup{out}}$
nulla e dopo aver realizzato questa condizione abbiamo commutato l'interruttore
su $S_{2}$.  Nella tabella~\ref{tab:dati} sono riportati i dati che abbiamo
raccolto.  Come incertezza massima sulle tensioni $V_{\textup{out}}$ abbiamo
assunto un decimo della scala utilizzata, vale a dire metà di ciascun quadratino
visibile sullo schermo dell'oscilloscopio.  Con il software ROOT, abbiamo
adattato i dati alla funzione
\begin{equation}
  V_{\textup{out}}(n) = V_{0}\e^{-\alpha n}+k,
\end{equation}
con $V_{0}$, $\alpha$ e $k$ parametri liberi e $n$ numero di fogli.  I risultati
del fit sono visibili nella figura~\ref{fig:fit1}.  Il $\chi^{2}$ ridotto è
molto elevato, superiore a \num{10}, allora abbiamo ripetuto il fit eliminando i
primi tre dati, che si discostano in maniera evidente dalla curva teorica, in
particolare i primi due.  I risultati di questo secondo fit sono visibili nella
figura~\ref{fig:fit2}.  Il valore del $\chi^{2}$ ridotto di questo fit è circa
\num{0.75}.  Il coefficiente di attenuazione $\alpha$ vale \num{0.207(15)}.
Abbiamo misurato con un calibro a cursore ventesimale lo spessore di dieci
foglietti di carta ottenendo $L_{10} = \SI{0.60(5)}{\milli\metre}$, quindi lo
spessore di un singolo foglio è $L = \SI{0.060(5)}{\milli\metre}$.  In questo
modo possiamo ricavare il coefficiente di assorbimento dei foglietti espresso in
\si[per-mode=reciprocal]{\per\centi\metre}:
\begin{equation}
  \alpha' = \frac{\alpha}{L} =
  \SI[per-mode=reciprocal]{34.50(54)}{\per\centi\metre}.
\end{equation}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{fit1}
  \caption{Primo fit.}
  \label{fig:fit1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{fit2}
  \caption{Secondo fit.}
  \label{fig:fit2}
\end{figure}

Infine abbiamo studiato il funzionamento dell'integratore.  Inserendo in
ingresso a questo stadio, sul resistore $R_{32}$, un segnale sinusoidale con
frequenza $f = \SI{1}{\kilo\hertz}$ e poi un altro con $f = \SI{100}{\hertz}$
abbiamo ottenuto in uscita un segnale $V_{\textup{out}}$ costante (e in
particolare nullo).  Queste frequenze sono molto più grandi della frequenza di
polo dell'integratore
$f_{\textup{p}} = 1/(2\pi C_{9} R_{34}) = \SI{0.22}{\hertz}$ e per
$f \gg f_{\textup{p}}$ l'integratore effettua la media del segnale di ingresso
(che per un seno è uguale a $0$).  Inviando invece in ingresso un'onda
sinusoidale con frequenza $f = \SI{10}{\hertz}$ in uscita abbiamo ottenuto
un'onda sinusoidale un po' amplificata, poiché per frequenze confrontabili con
quella di polo l'integratore si comporta come un amplificatore invertente con
amplificazione $-R_{34}/R_{32}$.  Abbiamo sostituito alla resistore $R_{32}$ con
un altro di resistenza nominale $R'_{32} = \SI{4.7}{\kilo\ohm}$ e, sempre
mantenendo in ingresso un segnale sinusoidale di frequenza $f =
\SI{10}{\hertz}$,
abbiamo osservato, mediante l'oscilloscopio, che in uscita c'era un'onda quadra.
Questo è dovuto al fatto che il rapporto di amplificazione è ora
$-R_{34}/R'_{32} \approx 700$, quindi l'amplificatore operazionale è in
saturazione.
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
